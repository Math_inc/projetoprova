package br.edu.ifsc.ProjetoProva;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class activity_HistoriaCampus extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__historia_campus);

    }

    public void btnVoltar(View view) {
        Intent btnVoltar = new Intent(activity_HistoriaCampus.this, MainActivity.class);
        startActivity(btnVoltar);
    }
}
