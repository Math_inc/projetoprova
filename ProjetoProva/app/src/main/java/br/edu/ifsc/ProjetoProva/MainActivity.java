package br.edu.ifsc.ProjetoProva;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button campus = (Button) findViewById(R.id.HC);
        campus.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent campus = new Intent(MainActivity.this, activity_HistoriaCampus.class);
                startActivity(campus);
            }

        });
        Button cursoT = (Button) findViewById(R.id.CT);
        cursoT.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent cursoT = new Intent(MainActivity.this, activity_CursoTecnico.class);
                startActivity(cursoT);
            }

        });
        Button cursoS = (Button) findViewById(R.id.CS);
        cursoS.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent cursoS = new Intent(MainActivity.this, activity_CursoSuperior.class);
                startActivity(cursoS);
            }

        });
    }
}